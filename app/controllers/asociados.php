<?php
require_once "utils/utils.php";
require_once "utils/File.php";
require_once "entity/Asociado.php";

$name = "";
$message = "";
$description = "";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (empty($_POST['nombre'])) {
        $errores []= 'Introduce tu nombre';
    } else {
        try {
            $name = trim(htmlspecialchars($_POST["nombre"]));
            $description = trim(htmlspecialchars($_POST["descripcion"]));
            $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];
            $imagen = new File("imagen", $tiposAceptados);
            $imagen->saveUploadFile(Asociado::RUTA_IMAGE_LOGO);
            $message = "Datos enviados";
        } catch (FileException $fileException) {
            $errores []= $fileException->getMessage();
        }
    }
}

require_once __DIR__ . '/../views/asociados.view.php';
