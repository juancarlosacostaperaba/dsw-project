<?php
require_once "utils/utils.php";
$errores = array(); //array para almacenar los errores

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (empty($_POST['nombre']) == true) {
        array_push($errores, 'Introduce tu nombre');
    }

    if (empty($_POST['mail']) == true) {
        array_push($errores, 'Introduce tu email');
    } elseif (filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL) == false) {
        array_push($errores, 'Introduce un email válido');
    }

    if (empty($_POST['tema']) == true) {
        array_push($errores, 'Introduce un tema');
    }
}

require_once __DIR__ . "/../views/contact.view.php";
