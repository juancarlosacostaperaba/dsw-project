<?php
// ---------------------------------------------- //
require_once "database/IEntity.php";
require_once "database/Connection.php";
require_once "database/QueryBuilder.php";
// ---------------------------------------------- //
require_once "utils/utils.php";
// ---------------------------------------------- //
require_once "entity/ImagenGaleria.php";
//require_once "entity/Categoria.php";
require_once "entity/Asociado.php";
// ---------------------------------------------- //
require_once "exceptions/FileException.php";
require_once "exceptions/QueryException.php";
require_once "exceptions/AppException.php";
// ---------------------------------------------- //
require_once "core/App.php";
// ---------------------------------------------- //
require_once "repository/ImagenGaleriaRepository.php";
// ---------------------------------------------- //

try {
    $connection = App::getConnection();

    $imagenGaleriaRepository = new ImagenGaleriaRepository();
    $arrayImagenes = $imagenGaleriaRepository->findAll();
} catch (QueryException $queryException) {
    $errores [] = $queryException->getMessage();
    throw new $queryException("No se hacer la consulta a la BBDD");
} catch (AppException $appException) {
    $errores [] = $appException->getMessage();
    throw new $appException("No se ha podido conectar con la BBDD");
}

$asociados = [];
for ($i=0; $i < 7; $i++) {
    $asociados[$i] = new Asociado("Asociado ".($i + 1), ($i + 1), "asociado ".($i + 1));
}
if (count($asociados) <= 3) {
    $asociados;
} elseif (count($asociados) > 3) {
    $asociados = remueve($asociados);
}

require_once __DIR__ . "/../views/index.view.php";
