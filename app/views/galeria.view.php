<?php
// Importamos el partial que contiene la apertura del HTML y la etiqueca head con todos los link enlazados
include __DIR__ . "/partials/inicio-doc.partials.php";

// Importamos el partial que contiene el menú de navegación
include __DIR__ . "/partials/nav.partials.php";
?>
<!-- Principal Content Start -->
<div id="galeria">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
            <h1>GALERÍA</h1>
            <hr>
            <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') : //condicional if que comprueba que la petición que se hace al servidor sea de método post una vez enviado el formulario?>
            <div class="alert alert-<?= empty($errores) ? 'info' : 'danger'; //comprobación de que el array errores esté vacío. Si está vacío, devuelve FALSE y no se cumple el echo de algunas de los strings que tiene a continuación ('info' y 'danger')?> alert-dismissible" role="alert">
                <button type="button" class="clase" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <?php if (empty($errores)) : //condicional if que comprueba si el array errores se encuentra vacío o no?>
                <p><?= $mensaje //echo de la variable mensaje, que es donde se almacena?></p>
              <?php else : //condicional else?>
                <ul>
                    <?php foreach ($errores as $error) : //bucle foreach que recorre todos los eleemntos del array errores?>
                    <li><?= $error //echo que muetra los errores que se encuentran dentro del array de errores?></li>
                  <?php endforeach; //cierre del bucle foreach?>
                </ul>
              <?php endif; //cierre del segundo condicional if, además de cerrar el else también?>
            </div>
          <?php endif; //cierre del primer condicional if?>

            <form class="form-horizontal" action="<?=$_SERVER["REQUEST_URI"] //echo que imprime el nombre del archivo que está ejecutándose?>" method="POST"
                enctype="multipart/form-data">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Imagen</label>
                        <input class="form-control-file" name="imagen" type="file">
                    </div>
                </div>
                   <label class="label-control">Categoría</label>
                   <select class="form-control" name="categoria">
                     <?php foreach ($categorias as $categoria) : ?>
                     <option value="<?= $categoria->getId(); ?>"><?= $categoria->getNombre(); ?></option>
                     <?php endforeach; ?>
                   </select>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Descripción</label>
                        <textarea class="form-control" name="descripcion" value="<?= $descripcion //echo de la variable description?>"></textarea>
                        <button class="pull-right btn btn-lg sr-button">ENVIAR</button>
                    </div>
                </div>
            </form>
        </div>
        <table class="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Imagen</th>
              <th scope="col">Nombre</th>
              <th scope="col">Categoría</th>
              <th scope="col">Descripción</th>
              <th scope="col">Visualizaciones</th>
              <th scope="col">Likes</th>
              <th scope="col">Downloads</th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach ($arrayImagenes as $imagen) {
                ?>
            <tr>
              <td><?= $imagen->getId(); ?></td>
              <td>
                <img src="<?= ImagenGaleria::RUTA_IMAGENES_PORTFOLIO.$imagen->getNombre(); ?>" alt="<?= $imagen->getDescripcion(); ?>" class="foto200px">
              </td>
              <td><?= $imagen->getNombre(); ?></td>
              <td><?= $imagenGaleriaRepository->getCategoria($imagen)->getNombre() ?></td>
              <td><?= $imagen->getDescripcion(); ?></td>
              <td><?= $imagen->getNumVisualizaciones(); ?></td>
              <td><?= $imagen->getNumLikes(); ?></td>
              <td><?= $imagen->getNumDownloads(); ?></td>
            </tr>
            <?php
            } ?>
          </tbody>
      </table>
    </div>
</div>
<!-- Principal Content End -->
<?php
// Importamos el partial que contiene los scripts de js y el cierre de las etiquetas body y html
include __DIR__ . "/partials/fin-doc.partials.php";
?>
