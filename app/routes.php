<?php
return [
 "A16/dsw-project" => "app/controllers/index.php",
 "A16/dsw-project/index" => "app/controllers/index.php",
 "A16/dsw-project/about" => "app/controllers/about.php",
 "A16/dsw-project/asociados" => "app/controllers/asociados.php",
 "A16/dsw-project/blog" => "app/controllers/blog.php",
 "A16/dsw-project/contact" => "app/controllers/contact.php",
 "A16/dsw-project/galeria" => "app/controllers/galeria.php",
 "A16/dsw-project/post" => "app/controllers/single_post.php"
];
