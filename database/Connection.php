<?php
/**
 * [Connection description]
 */
class Connection
{

  /**
   * [make description]
   * @return [type] [description]
   */
    public static function make()
    {
        $config = App::get("config")["database"];
        try {
            $pdo = new PDO(
                $config['connection'].';dbname='.$config['name'],
                $config['username'],
                $config['password'],
                $config['options']
      );
        } catch (\Exception $errores) {
            $errores->getMessage();
        }
        return $pdo;
    }
}
