<?php
/**
 * Clase QueryBuilder
 */
abstract class QueryBuilder
{
    private $connection;
    private $table;
    private $classEntity;

    /**
     *
     * @param string $table
     * @param string $classEntity
     */
    public function __construct(string $table, string $classEntity)
    {
        $this->connection = App::getConnection();
        $this->table = $table;
        $this->classEntity = $classEntity;
    }

    /**
     * @param IEntity $entity
     */
    public function save(IEntity $entity): void
    {
        try {
            $parameters = $entity->toArray();
            $sql = sprintf(
          "insert into %s (%s) values (%s)",
          $this->table,
          implode(", ", array_keys($parameters)),
          ":". implode(", :", array_keys($parameters))
    );
            $statement = $this->connection->prepare($sql);
            $statement->execute($parameters);
        } catch (PDOException $exception) {
            throw new QueryException("Error al insertar en la BBDD.");
        }
    }

    /**
     * @param  string $sql
     * @return array
     */
    public function executeQuery(string $sql): array
    {
        $pdoStatement = $this->connection->prepare($sql);
        if ($pdoStatement->execute()===false) {
            throw new QueryException("No se ha podido ejecutar la consulta");
        }
        return $pdoStatement->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, $this->classEntity);
    }

    /**
     * @param  int     $id
     * @return IEntity
     */
    public function find(int $id): IEntity
    {
        $sql = "SELECT * FROM $this->table WHERE id=$id";
        $result = $this->executeQuery($sql);
        if (empty($result)) {
            throw new NotFoundException("No se ha encontrado el elemento con id $id");
        }
        return $result[0];
    }

    /**
     * Método para devolver en forma de array las imágenes que se envían por el formulario de la página galeria.php
     * @return array
     */
    public function findAll()
    {
        // Sentencia SQL, dónde se hace un SELECT de la tabla que se pasa por parámetro
        $sql = "select * from $this->table";
        $pdoStatement = $this->executeQuery($sql);
        return $pdoStatement;
    }
}
