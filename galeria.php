<?php
// ---------------------------------------------- //
require_once "database/IEntity.php";
require_once "database/Connection.php";
require_once "database/QueryBuilder.php";
// ---------------------------------------------- //
require_once "utils/utils.php";
require_once "utils/File.php";
// ---------------------------------------------- //
require_once "entity/ImagenGaleria.php";
require_once "entity/Categoria.php";
// ---------------------------------------------- //
require_once "exceptions/FileException.php";
require_once "exceptions/QueryException.php";
// ---------------------------------------------- //
require_once "core/App.php";
// ---------------------------------------------- //
require_once "repository/ImagenGaleriaRepository.php";
require_once "repository/CategoriaRepository.php";
// ---------------------------------------------- //

$descripcion = "";
$mensaje = "";

try {
  $config = require_once("app/config.php");
  App::bind("config", $config);
  $connection = App::getConnection();

  $imagenGaleriaRepository = new ImagenGaleriaRepository();
  $categoriaRepository = new CategoriaRepository();

  if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $categoria = trim(htmlspecialchars($_POST["categoria"]));
    $descripcion = trim(htmlspecialchars($_POST["descripcion"]));
    $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];
    $imagen = new File("imagen", $tiposAceptados);
    $imagen->saveUploadFile(ImagenGaleria::RUTA_IMAGENES_GALLERY);
    $imagen->copyFile(ImagenGaleria::RUTA_IMAGENES_GALLERY, ImagenGaleria::RUTA_IMAGENES_PORTFOLIO);

    $imagenGaleria = new ImagenGaleria($imagen->getFileName(), $categoria, $descripcion);
    $imagenGaleriaRepository->save($imagenGaleria);

    $mensaje = "Datos enviados";
  }

  $arrayImagenes = $imagenGaleriaRepository->findAll();
  $categorias = $categoriaRepository->findAll();

}
catch (FileException $fileException) {
  $errores [] = $fileException->getMessage();
  throw new $fileException("No se ha podido guardar la imagen");
}
catch (QueryException $queryException) {
  $errores [] = $queryException->getMessage();
  throw new $queryException("No se hacer la consulta a la BBDD");
}
catch (AppException $appException) {
  $errores [] = $appException->getMessage();
  throw new $appException("No se ha podido conectar con la BBDD");
}

require "views/galeria.view.php";
?>
