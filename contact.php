<?php
require "utils/utils.php";
$errores = array(); //array para almacenar los errores

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  if (empty($_POST['nombre']) == True) {
    array_push($errores,'Introduce tu nombre');
  }

  if (empty($_POST['mail']) == True) {
    array_push($errores,'Introduce tu email');
  } elseif (filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL) == False) {
    array_push($errores,'Introduce un email válido');
  }

  if (empty($_POST['tema']) == True) {
    array_push($errores,'Introduce un tema');
  }
}

require "views/contact.view.php";
?>
