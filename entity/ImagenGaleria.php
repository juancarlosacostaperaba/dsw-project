<?php
require_once "database/IEntity.php";

/**
 * Clase ImagenGaleria
 */
class ImagenGaleria implements IEntity
{

    //      PROPIEDADES     //
    private $id;
    private $nombre;
    private $categoria;
    private $descripcion;
    private $numVisualizaciones;
    private $numLikes;
    private $numDownloads;

    //      CONSTRUCTOR     //
    /**
     * @param integer $id
     * @param string  $nombre
     * @param integer $categoria
     * @param string  $descripcion
     * @param integer $numVisualizaciones
     * @param integer $numLikes
     * @param integer $numDownloads
     */
    public function __construct(string $nombre = "", $categoria = 1, string $descripcion = "", $numVisualizaciones = 0, $numLikes = 0, $numDownloads = 0)
    {
        $this->id = null;
        $this->nombre = $nombre;
        $this->categoria = $categoria;
        $this->descripcion = $descripcion;
        $this->numVisualizaciones = $numVisualizaciones;
        $this->numLikes = $numLikes;
        $this->numDownloads = $numDownloads;
    }

    //      CONSTANTES      //
    const RUTA_IMAGENES_PORTFOLIO = "images/index/portfolio/";
    const RUTA_IMAGENES_GALLERY = "images/index/gallery/";

    //  GETTERS & SETTERS   //
    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
    * Get the value of Nombre
    *
    * @return mixed
    */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
    * Set the value of Nombre
    *
    * @param mixed nombre
    *
    * @return self
    */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
    * Get the value of Descripcion
    *
    * @return mixed
    */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
    * Set the value of Descripcion
    *
    * @param mixed descripcion
    *
    * @return self
    */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
    * Get the value of Num Visualizaciones
    *
    * @return mixed
    */
    public function getNumVisualizaciones()
    {
        return $this->numVisualizaciones;
    }

    /**
    * Set the value of Num Visualizaciones
    *
    * @param mixed numVisualizaciones
    *
    * @return self
    */
    public function setNumVisualizaciones($numVisualizaciones)
    {
        $this->numVisualizaciones = $numVisualizaciones;
        return $this;
    }

    /**
    * Get the value of Num Likes
    *
    * @return mixed
    */
    public function getNumLikes()
    {
        return $this->numLikes;
    }

    /**
    * Set the value of Num Likes
    *
    * @param mixed numLikes
    *
    * @return self
    */
    public function setNumLikes($numLikes)
    {
        $this->numLikes = $numLikes;
        return $this;
    }

    /**
    * Get the value of Num Downloads
    *
    * @return mixed
    */
    public function getNumDownloads()
    {
        return $this->numDownloads;
    }

    /**
    * Set the value of Num Downloads
    *
    * @param mixed numDownloads
    *
    * @return self
    */
    public function setNumDownloads($numDownloads)
    {
        $this->numDownloads = $numDownloads;
        return $this;
    }

    /**
     * Get the value of Categoria
     *
     * @return mixed
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set the value of Categoria
     *
     * @param mixed $categoria
     *
     * @return self
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    //        MÉTODOS       //
    /**
     * @return string ruta portfolio
     */
    public function getURLPortfolio() : string
    {
        return self::RUTA_IMAGENES_PORTFOLIO . $this->getNombre();
    }

    /**
     * @return string ruta galería
     */
    public function getURLGallerry() : string
    {
        return self::RUTA_IMAGENES_GALLERY . $this->getNombre();
    }

    /**
     * @return array array con datos
     */
    public function toArray(): array
    {
        return [
     "id"=>$this->getId(),
     "nombre"=>$this->getNombre(),
     "categoria"=>$this->getCategoria(),
     "descripcion"=>$this->getDescripcion(),
     "numVisualizaciones"=>$this->getNumVisualizaciones(),
     "numLikes"=>$this->getNumLikes(),
     "numDownloads"=>$this->getNumDownloads()
    ];
    }
}
