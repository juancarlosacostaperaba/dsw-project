<?php
require_once __DIR__ .  "/../exceptions/FileException.php";

// Clase
class File
{
    private $file;
    private $fileName;

    // Constructor
    public function __construct(string $fileName, array $arrTypes)
    {
        $this->file = $_FILES[$fileName];
        $this->fileName = "";

        if (($this->file["name"] == "")) {
            throw new FileException("Debes especificar un fichero", 1);
        }

        if ($this->file["error"] !== UPLOAD_ERR_OK) {
            switch ($this->file["error"]) {
        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_FORM_SIZE:
          throw new FileException("Este archivo es demasiado pesado.", 1);
        case UPLOAD_ERR_PARTIAL:
          throw new FileException("El archivo está incompleto o dañado.", 1);
        default:
          throw new FileException("Error genérico en la subida del fichero.", 1);
          break;
      }
        }

        if (in_array($this->file["type"], $arrTypes) === false) {
            throw new FileException("El formato de la foto no es el correcto.", 1);
        }
    }

    // Getters
    /**
    * Get the value of fileNombre
    *
    * @return mixed
    */
    public function getFileName()
    {
        return $this->fileName;
    }

    // Métodos
    public function saveUploadFile($ruta)
    {
        if (is_uploaded_file($this->file['tmp_name']) === true) {
            $rutaNombre = $ruta.$this->file['name'];
            if (is_file($rutaNombre) !== false) {
                $idUnico = time();
                $this->file['name'] = $idUnico.'_'.$this->file['name'];
                $rutaNombre = $ruta.$this->file['name'];
            }
            $this->fileName = $this->file['name'];
            if (move_uploaded_file($this->file['tmp_name'], $rutaNombre) === false) {
                throw new FileException("No se ha podido mover el archivo a la ruta especificada.", 1);
            }
        } else {
            throw new FileException("El archivo no se ha subido mediante un formulario.", 1);
        }
    }

    public function copyFile($origen, $destino)
    {
        $origen = $origen.$this->file['name'];
        $destino = $destino.$this->file['name'];
        if (is_file($origen) === false) {
            throw new FileException("No existe el fichero $origen.", 1);
        }
        if (is_file($destino) === true) {
            throw new FileException("El fichero $destino ya existe.", 1);
        }
        if (copy($origen, $destino) === false) {
            throw new FileException("No se ha podido copiar el archivo.", 1);
        }
    }
}
