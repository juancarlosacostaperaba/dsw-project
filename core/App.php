<?php
/**
 *
 */
class App
{
    //Almacena los objetos de nuestro contenedor
    private static $container = [];

    /**
     * Método bind set
     * @param  string $key
     * @param  $value
     */
    public static function bind(string $key, $value)
    {
        static::$container[$key] = $value;
    }

    /**
     * Método get
     * @param  string $key
     */
    public static function get(string $key)
    {
        if (!array_key_exists($key, static::$container)) {
            throw new AppException("No se ha encontrado la clave $key en el contenedor.");
        }
        return static::$container[$key];
    }

    /**
     * Método getConnection
     */
    public static function getConnection()
    {
        if (!array_key_exists("connection", static::$container)) {
            static::$container["connection"] = Connection::make();
        }
        return static::$container["connection"];
    }
}
