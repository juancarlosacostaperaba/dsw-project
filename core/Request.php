<?php
/**
 * Clase Request
 */
class Request
{
    /**
     * [uri description]
     * @return [type] [description]
     */
    public static function uri()
    {
        return trim(parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH), "/");
    }
}
