<?php
// Clase
class FileException extends Exception
{
    // Constructor
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}
