<?php
/**
 *
 */
class AppException extends Exception
{

  /**
   * Constructor
   *
   * @param string $message
   */
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}
