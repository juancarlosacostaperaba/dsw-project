<?php
/**
 * Excepción personalizada de la clase QueryException
 */
class QueryException extends Exception
{

  /**
   * Constructor
   *
   * @param string $message
   */
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}
